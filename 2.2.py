r = float(input('Введите радиус окружности R\n'))
x = float(input('Введите координату точки x\n'))
y = float(input('Введите координату точки y\n'))
if ((r*x+r*y+r**2 == 0 and x <= 0 and y <= 0) or (x**2+y**2 <= r**2 and x >= 0 and y >= 0)):
    print('Попадает')
else:
    print('Не попадает')
