import tkinter
import requests
from tkinter import messagebox

class Scraper:
    def __init__(self):
        pass

    def getHTML():
        domain = Scraper.field.get()
        url = Scraper.base + domain
        
        try:
            result = requests.get(url)
            result.encoding = 'UTF-8'
            
            response = tkinter.Tk()
            response.title(f'Ответ {domain}')
            response.geometry('800x500')

            def copyHTML():
                response.clipboard_clear()
                response.clipboard_append(result.text)
                response.update()
                messagebox.showinfo('Копирование', 'Текст скопирован в буфер обмена!')

            def createHTML():
                html = open(f'{domain}.html', 'w', encoding = 'utf-8')
                html.write(result.text)
                html.close()
                messagebox.showinfo('Создание файла', f'Файл {domain}.html успешно создан!')
                

            scroll = tkinter.Scrollbar(response)
            scroll.pack(side = 'right', fill = 'y')
            
            response_text = tkinter.Text(response, yscrollcommand = scroll.set)
            response_text.insert(1.0, result.text)
            response_text.pack()

            scroll.config(command = response_text.yview)

            copy_button = tkinter.Button(response, text = 'Копировать', command = copyHTML)
            copy_button.pack()

            create_button = tkinter.Button(response, text = 'Создать HTML файл', command = createHTML)
            create_button.pack()
            
            response.mainloop()
        except requests.exceptions.ReadTimeout:
            messagebox.showerror('Ошибка', 'Превышено время ожидания')
        except requests.exceptions.ConnectTimeout:
            messagebox.showerror('Ошибка', 'Превышено время ожидания')
        except requests.exceptions.ConnectionError:
            messagebox.showerror('Ошибка', 'Ошибка соединения')
        except requests.exceptions.HTTPError:
            messagebox.showerror('Ошибка', 'Ошибка HTTP')
        except requests.exceptions.InvalidURL:
            messagebox.showerror('Ошибка', 'Неправильный хост')

    def show_about():
        messagebox.showinfo('О программе', 'Версия 1.0')

    base = 'http://'

    root = tkinter.Tk()
    root.title('Веб-скрапер')
    root.geometry('600x350')

    head_menu = tkinter.Menu(root)
    head_menu.add_command(label = 'О программе', command = show_about)
    root.config(menu = head_menu)

    header = tkinter.Label(root, pady = 20, text = 'Введите URL вида \'example.com\'')
    header.pack()

    field = tkinter.StringVar()
    
    field_entry = tkinter.Entry(root, textvariable = field, width = 30)
    field_entry.pack()
        
    btn = tkinter.Button(root, text = 'Scrap!', command = getHTML)
    btn.pack()
    
if __name__ == '__main__':
    Scraper.root.mainloop()
