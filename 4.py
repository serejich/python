import numpy as np
import random
import math
multiply = 1
sum = 0
n = int(input('Введите количество элементов массива \n'))
arr = np.random.uniform(-10, 10, (n))
min = min(arr)
max = max(arr)
print(f'arr: {arr}')
print(f'min(arr): {min}')
print(f'max(arr): {max}')
y = int(input('Введите y, которое соответствует неравенству "min(arr) < y < max(arr)" \n'))
if (y > min and y < max):
    for i in arr:
        if (math.fabs(i) > y):
            print(f'Значение модуля i больше y: {i}')
            multiply *= i
        else:
            print(f'Значение модуля i меньше y {i}')
            sum += math.fabs(i)
    print(f'Произведение: {multiply}')
    print(f'Сумма: {sum}')
else:
    print('Значение y не соответствует неравенству!')
