import math
x = float(input('Введите значение x\n'))
if x < -6:
    y = 0
elif x > -6 and x < -2:
    y = ((-8/9)*math.pow(x, 2))-((56*x)/9)-(80/9)
elif x >= -2 and x < 2:
    y = (math.pow(x, 2)/2)-2
elif x >= 2 and x < 8:
    y = math.log(x-1, 2)
elif x >= 8:
    y = -2*x+18
print('x = {0:.2f} y = {1:.2f}'.format(x, y))
