from math import *
a = float(input('Введите значение a\n'))
def ctg(a):
    return 1/tan(a)
try:
    z1 = (sin(a)**2-tan(a)**2)/(cos(a)**2-ctg(a)**2)
    z2 = tan(a)**6
    print('a = {0:.2f} z1 = {1:.2f} z2 = {2:.2f}'.format(a, z1, z2))
except ZeroDivisionError:
    print('Знаменатель в выражении z1 равен нулю!')
